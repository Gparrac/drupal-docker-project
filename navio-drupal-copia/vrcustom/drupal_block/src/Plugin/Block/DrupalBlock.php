<?php

namespace Drupal\drupal_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * @Block(
 *   id = "DrupalBlock",
 *   admin_label = @Translation("DrupalBlock"),
 *   category = @translation("Drupal Block"),
 * )
 */
class DrupalBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(){
    return [
      '#type' => 'markup',
      '#markup' => 'Test bloque!!',
    ];
  }

}