<?php
namespace Drupal\drupal_block\Controller;
use Drupal\Core\Controller\ControllerBase;
 
class PruebasController extends ControllerBase {
     
/* Método acción content devuelve directamente un contenido en html,
este método será usado en una ruta */
  public function content() {
    return array(
        '#type' => 'markup',
        '#markup' => $this->t('Hola mundo !!'),
    );
  }
   
}
 
?>